import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/User.model';
import { LoginProvider } from '../providers/login.provider';

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
  authenticated;
  user;
  loginData;
  constructor(
    private router: Router,
    private loginProvider: LoginProvider
  ) { }

  ngOnInit() {
    localStorage.getItem('user') ? this.authenticated = true : this.authenticated = false;
    this.loginData = {
      username: '',
      password: ''
    };
  }

  navigateTo(url: string) {
    this.router.navigate([url]);
  }

  login() {
    //Demo user until cors problem is solved
    // this.user={
    //   name:'projects/mal-client-demo/databases/(default)/documents/users/wkYlDlG3RlEoqFMsm5hs'
    // };
    // localStorage.setItem('user', JSON.stringify(this.user));
    // this.closeLoginDialog();
    // this.authenticated = true; 
    // this.loginData = {
    //   username: '',
    //   password: ''
    // };

    //Real login
    this.loginProvider
      .login(this.loginData.username, this.loginData.password)
      .subscribe((user) => {
        if (user[0].document) {
          this.user = new User(
            user[0].document.fields.email.stringValue,
            user[0].document.fields.username.stringValue,
            user[0].document.name
          );
          localStorage.setItem('user', JSON.stringify(this.user));
          this.closeLoginDialog();
          this.authenticated = true;
          this.loginData = {
            username: '',
            password: ''
          };
        }
      });
  }

  logOut() {
    this.authenticated = false;
    localStorage.removeItem('user');
    this.user = null;
    this.navigateTo('');
  }

  //Login dialog functions

  openLoginDialog() {
    let dialog = document.getElementById('loginDialog');
    dialog.style.display = 'block'
  }

  closeLoginDialog() {
    let dialog = document.getElementById('loginDialog');
    dialog.style.display = 'none'
  }

}
