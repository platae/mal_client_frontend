import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AnimeCarrouselComponent } from './components/anime-carrousel-component/anime-carrousel-component.component';
import { AnimeDetailComponent } from './anime-detail/anime-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AnimeProvider } from './providers/anime.provider';
import { FormsModule } from '@angular/forms';
import { HistoryProvider } from './providers/history.provider';
import { HttpClientModule } from '@angular/common/http';
import { LoaderComponent } from './components/loader/loader.component';
import { LoginProvider } from './providers/login.provider';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { PageHeaderComponent } from './page-header/page-header.component';
//import { ProfileModule } from './profile/profile.module';
import { UsersModule } from './users/users.module';
import { UsersProvider } from './providers/users.provider';
import { ProfileComponent } from './profile/profile.component';
import { HistoryManagerComponent } from './history-manager/history-manager.component';

@NgModule({
  declarations: [
    AppComponent,
    AnimeCarrouselComponent,
    AnimeDetailComponent,
    MainScreenComponent,
    LoaderComponent,
    PageHeaderComponent,
    ProfileComponent,
    HistoryManagerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    // ProfileModule,
    UsersModule
  ],
  exports:[
    AnimeCarrouselComponent,
    LoaderComponent
  ],
  providers: [
    AnimeProvider,
    HistoryProvider,
    LoginProvider,
    UsersProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
