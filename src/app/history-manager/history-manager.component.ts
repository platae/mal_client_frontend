import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { History } from '../models/History.model';
import { HistoryProvider } from '../providers/history.provider';
import { SCORES, STATUS } from '../app.constants';
import { Anime } from '../models/Anime.model';
import { UsersProvider } from '../providers/users.provider';

@Component({
  selector: 'history-manager',
  templateUrl: './history-manager.component.html',
  styleUrls: ['./history-manager.component.scss']
})
export class HistoryManagerComponent implements OnInit {
  //If received it will patch the element
  @Input() receivedHistory: History;
  @Input() anime: Anime;
  @Output() updatedHistory = new EventEmitter();
  history;

  loading;
  //Constants
  animeStatus;
  scores = SCORES;

  constructor(
    private historyProvider: HistoryProvider,
    private usersProvider: UsersProvider
  ) { }

  ngOnInit() {
    if (this.receivedHistory) {
      this.history = this.receivedHistory;
    }
    else {
      this.history = new History(
        this.anime.mal_id,
        this.anime.title,
        this.anime.image_url,
        0,
        'PENDING',
        0,
        null,
        null
      );
    }
    this.loadStatus();
  }

  addToHistory() {
    this.loading = true;
    this.history = new History(
      this.history.mal_id,
      this.history.title,
      this.history.image_url,
      this.history.score,
      this.history.status,
      this.history.watched_episodes,
      null,
      null
    );
    this.historyProvider
      .add(null, this.usersProvider.getUser().name, this.history)
      .subscribe((response) => {
        this.loading = false;
        this.updatedHistory.emit(JSON.stringify(response));
        this.receivedHistory= new History(
          null,
          null,
          null,
          null,
          null,
          null,
          response.fields,
          response.name
        );
      });
  }

  updateHistory() {
    this.loading = true;
    this.history = new History(
      this.history.mal_id,
      this.history.title,
      this.history.image_url,
      this.history.score,
      this.history.status,
      this.history.watched_episodes,
      null,
      this.history.name
    );
    this.historyProvider
      .modify(this.history.name,
        null,
        null,
        this.history)
      .subscribe((updatedHistory) => {
        this.loading = false;
        this.updatedHistory.emit(JSON.stringify(updatedHistory));
      });
  }

  selectedStatus() {
    if (this.history.status === 'FINISHED') {
      this.history.watched_episodes = this.anime.episodes;
    }
    else {
      this.history.watched_episodes = 0;
    }
  }

  //Private status
  private loadStatus() {
    switch (this.anime.status) {
      case 'Not yet aired':
        this.animeStatus = ['PENDING'];
        break;
      case 'Currently Airing':
        this.animeStatus = ['PENDING', 'WATCHING'];
        break;
      default:
        this.animeStatus = STATUS;
        break;
    }
  }

}
