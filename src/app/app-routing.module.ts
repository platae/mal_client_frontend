import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//Developed components
import { AnimeDetailComponent } from './anime-detail/anime-detail.component';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
  { path: '', component: MainScreenComponent },
  { path: 'detail/:id', component: AnimeDetailComponent },
  { path: 'history', component: ProfileComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
