import { Component, OnInit } from '@angular/core';
import { AnimeProvider } from '../providers/anime.provider';
import { Season } from '../models/Season.model';
import { AnimeCategory } from '../models/AnimeCategory.model';
import { Router } from '@angular/router';

@Component({
  selector: 'main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.scss']
})
export class MainScreenComponent implements OnInit {

  constructor(
    private animeProvider: AnimeProvider,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadSeasonAnime('summer', 2019);
     setTimeout(() => {
      this.loadTopAnime('airing');
      setTimeout(() => {
        this.loadTopAnime('bypopularity');
        setTimeout(() => {
          this.loadTopAnime('favorite');
          setTimeout(() => {
            this.loadTopAnime('upcoming');
          }, 4500);
        }, 4500);
      }, 4500);
     }, 4500);

  }

  animeLists = {
    thisSeason: {
      loading: true,
      series: []
    },
    airing: {
      loading: true,
      series: []
    },
    favorite: {
      loading: true,
      series: []
    },
    bypopularity: {
      loading: true,
      series: []
    },
    upcoming: {
      loading: true,
      series: []
    }
  };

  animeSelected(anime:number){
    var url = '/detail/'+anime;
    this.router.navigate([url]);
  }

  //Private functions
  private loadSeasonAnime(season: Season, year: number) {
    let list = this.animeLists.thisSeason;
    this.animeProvider
      .getBySeason(season, year)
      .subscribe(
        (animes) => {
          list.loading = false;
          list.series = animes['anime'];
        }
      );
  }

  private loadTopAnime(category: AnimeCategory) {
    let list = this.animeLists[category];
    this.animeProvider
      .getTopFromCategory(category)
      .subscribe(
        (animes) => {
          list.loading = false;
          list.series = animes['top'];
        }
      );
  }

}
