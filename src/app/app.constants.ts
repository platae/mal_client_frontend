export const URLS = Object.freeze({
    FIRESTORE: {
        API_KEY: 'AIzaSyDIuY-97aCxL5utuG6c_Y9Fdziq5OkvE8o',
        BASE_NAME: 'projects/mal-client-demo/databases/(default)/documents',
        HISTORY: 'history',
        USERS: 'users'
    },
    JIKAN: {
        ANIME: 'anime',
        CATEGORIES: {
            AIRING: 'airing',
            FAVORITE: 'favorite',
            POPULARITY: 'bypopularity',
            UPCOMING: 'upcoming'
        },
        SEASON: 'season',
        SEASONS: {
            SPRING: 'spring',
            SUMMER: 'summer',
            AUTUMN: 'fall',
            WINTER: 'winter'
        },
        TOP_ANIME: 'top/anime'
    }
});

export const SCORES = Object.freeze([
    {
        value: 1,
        verbose: 'Apalling'
    },
    {
        value: 2,
        verbose: 'Horrible'
    },
    {
        value: 3,
        verbose: 'Very bad'
    },
    {
        value: 4,
        verbose: 'Bad'
    },
    {
        value: 5,
        verbose: 'Average'
    },
    {
        value: 6,
        verbose: 'Fine'
    },
    {
        value: 7,
        verbose: 'Good'
    },
    {
        value: 8,
        verbose: 'Very good'
    },
    {
        value: 9,
        verbose: 'Great'
    },
    {
        value: 10,
        verbose: 'Masterpice'
    }
]);

export const STATUS = Object.freeze([
    'PENDING',
    'WATCHING',
    'FINISHED']
);

export const PAGINATION = Object.freeze({
    PAGE_SIZE: 20
});