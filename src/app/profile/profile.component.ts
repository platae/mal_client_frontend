import { Component, OnInit } from '@angular/core';
import { HistoryProvider } from '../providers/history.provider';
import { History } from '../models/History.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  historyFinished;
  historyPending;
  historyWatching;
  user;
  //Loaders
  historyLoader;
  constructor(
    private historyProvider: HistoryProvider,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.historyLoader = true;
    this.historyProvider
      .getByUser(null, this.user.name)
      .subscribe((history) => {
        this.historyLoader=false;
        this.treatResponse(history);
      });
  }

  animeSelected(anime:number){
    var url = '/detail/'+anime;
    this.router.navigate([url]);
  }

  //Private functions
  private treatResponse(response) {
    this.historyFinished = [];
    this.historyPending = [];
    this.historyWatching = [];
    let documents = response.documents;
    documents.forEach(element => {
      var history_element = new History(null, null, null, null, null,null, element.fields, element.name);
      //Dividing the historics given their status
      if (history_element.status === 'PENDING') {
        this.historyPending.push(history_element);
      }
      if (history_element.status === 'FINISHED') {
        this.historyFinished.push(history_element);
      }
      if (history_element.status === 'WATCHING') {
        this.historyWatching.push(history_element);
      }
    });
  }

}
