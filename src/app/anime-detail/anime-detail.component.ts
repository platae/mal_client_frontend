import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnimeProvider } from '../providers/anime.provider';
import { HistoryProvider } from '../providers/history.provider';
import { UsersProvider } from '../providers/users.provider';
import { History } from '../models/History.model';

@Component({
  selector: 'app-anime-detail',
  templateUrl: './anime-detail.component.html',
  styleUrls: ['./anime-detail.component.scss']
})
export class AnimeDetailComponent implements OnInit {
  anime;
  authenticated;
  loading;
  user;
  userHistory;
  constructor(
    private route: ActivatedRoute,
    private animeProvider: AnimeProvider,
    private historyProvider: HistoryProvider,
    private usersProvider: UsersProvider
  ) { }

  ngOnInit() {
    this.loadAnime(this.route.snapshot.paramMap.get('id'));
    this.user = this.usersProvider.getUser();
    this.user ? this.authenticated = true : this.authenticated = false;
    if (this.authenticated) {
      this.loading = true;
      this.historyProvider
        .getAnimeInHistory(
          parseInt(this.route.snapshot.paramMap.get('id')),
          this.user.name)
        .subscribe((response) => {
          this.loading = false;
          if (response[0].document) {
            this.userHistory = new History(null, null, null, null, null, null, response[0].document.fields, response[0].document.name);
          }
        });
    }
  }

  //Private functions
  private loadAnime(animeId) {
    this.animeProvider
      .getByID(animeId)
      .subscribe((anime) => {
        this.anime = anime;
      });
  }

}
