import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { URLS } from '../app.constants';
import { PAGINATION } from '../app.constants';
import { History } from '../models/History.model';

const baseUrl = environment.rest.firestore;

@Injectable()
export class HistoryProvider {
    constructor(
        private httpClient: HttpClient
    ) { };

    private animeQuery = {
        structuredQuery: {
            select: {
                fields: [
                    {
                        fieldPath: "image_url"
                    },
                    {
                        fieldPath: "mal_id"
                    },
                    {
                        fieldPath: "status"
                    },
                    {
                        fieldPath: "title"
                    },
                    {
                        fieldPath: "score"
                    },
                    {
                        fieldPath: "watched_episodes"
                    }
                ]
            },
            from: [
                {
                    collectionId: "history"
                }
            ],
            where: {
                fieldFilter: {
                    field: {
                        fieldPath: "mal_id"
                    },
                    op: "EQUAL",
                    value: {
                        integerValue: 5114
                    }
                }
            }
        }
    };

    getByUser(
        userId?: string,
        name?: string
    ): Observable<any> {
        var requestURL;
        if (name) {
            requestURL = baseUrl
                + '/' + name
                + '/' + URLS.FIRESTORE.HISTORY
                + '?pageSize=' + PAGINATION.PAGE_SIZE
                + '&key='
                + URLS.FIRESTORE.API_KEY;
        }
        else {
            if (userId) {
                requestURL = baseUrl
                    + '/' + URLS.FIRESTORE.BASE_NAME
                    + '/' + URLS.FIRESTORE.USERS
                    + '/' + userId
                    + '/' + URLS.FIRESTORE.HISTORY
                    + '?pageSize=' + PAGINATION.PAGE_SIZE
                    + '&key='
                    + URLS.FIRESTORE.API_KEY;
            }
        }

        return this.httpClient.get(requestURL);
    }

    //Recalls an anime inside a user history, 
    //Receives userId or user name (from firebase)
    //returns either the anime on the history or an empty object
    getAnimeInHistory(
        mal_id: number,
        name?: string,
        userId?: string
    ): Observable<any> {
        var requestURL;
        if (name) {
            requestURL = baseUrl
                + '/' + name
                + ':runQuery'
                + '?fields=document'
                + '&key='
                + URLS.FIRESTORE.API_KEY;
        }
        else {
            if (userId) {
                requestURL = baseUrl
                    + '/' + URLS.FIRESTORE.BASE_NAME
                    + '/' + URLS.FIRESTORE.USERS
                    + '/' + userId
                    + ':runQuery'
                    + '?fields=document'
                    + '&key='
                    + URLS.FIRESTORE.API_KEY;
            }
        }
        this.animeQuery.structuredQuery.where.fieldFilter.value.integerValue = mal_id;
        return this.httpClient.post(requestURL, this.animeQuery);
    }

    //Adds a history document to the user
    add(
        userId: string,
        name: string, //From the user
        history: History
    ): Observable<any> {
        var requestURL;
        if (userId) {
            requestURL = baseUrl
                + '/' + URLS.FIRESTORE.BASE_NAME
                + '/' + URLS.FIRESTORE.USERS
                + '/' + userId
                + '/' + URLS.FIRESTORE.HISTORY
                + '?fields=fields,name&key='
                + URLS.FIRESTORE.API_KEY;
        }
        if (name) {
            requestURL = baseUrl
                + '/' + name
                + '/' + URLS.FIRESTORE.HISTORY
                + '?fields=fields,name&key='
                + URLS.FIRESTORE.API_KEY;
        }
        else {
            throw new Error('function expects userId or name parameter.');
        }
        let historyToSend = {
            fields: history.fields
        };
        return this.httpClient.post(requestURL, JSON.stringify(historyToSend));
    }

    //Receives the name (firebase) or the object id and userId
    //Also the modified element is required
    modify(
        name: string,
        id: string,
        userId: string,
        history: History
    ): Observable<any> {
        var requestURL;
        if (name) {
            requestURL = baseUrl
                + '/' + name
                + '?fields=fields,name&key='
                + URLS.FIRESTORE.API_KEY;
        }
        else {
            if (id && userId) {
                requestURL = baseUrl
                    + '/' + URLS.FIRESTORE.BASE_NAME
                    + '/' + URLS.FIRESTORE.USERS
                    + '/' + userId
                    + '/' + URLS.FIRESTORE.HISTORY
                    + '/' + id
                    + '?fields=fields,name&key='
                    + URLS.FIRESTORE.API_KEY;
            }
            else {
                throw new Error('function expects name parameter, or id and userId parameters.');
            }
        }
        let historyToSend = {
            fields: history.fields
        };
        return this.httpClient.patch(requestURL,  JSON.stringify(historyToSend));
    }

    //Receives the name (firebase) or the element id and userId
    delete(
        name: string,
        id: string,
        userId: string
    ): Observable<any> {
        var requestURL;
        if (name) {
            requestURL = baseUrl
                + '/' + name
                + '?fields=fields,name&key='
                + URLS.FIRESTORE.API_KEY;
        }
        else {
            if (id && userId) {
                requestURL = baseUrl
                    + '/' + URLS.FIRESTORE.BASE_NAME
                    + '/' + URLS.FIRESTORE.USERS
                    + '/' + userId
                    + '/' + URLS.FIRESTORE.HISTORY
                    + '/' + id
                    + '?fields=fields,name&key='
                    + URLS.FIRESTORE.API_KEY;
            }
            else {
                throw new Error('function expects name parameter, or id and userId parameters.');
            }
        }
        return this.httpClient.delete(requestURL);
    }

}