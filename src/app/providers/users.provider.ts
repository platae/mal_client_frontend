import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { URLS } from '../app.constants';
import { NewUser } from '../models/newUser.model';

const baseUrl = environment.rest.firestore;

@Injectable()
export class UsersProvider {
    constructor(
        private httpClient: HttpClient
    ) { };

    create(
        user: NewUser
    ):Observable<any> {
        const requestURL = baseUrl
        +'/'+URLS.FIRESTORE.BASE_NAME
        +'/'+URLS.FIRESTORE.USERS
        +'?fields=fields,name&key='
        +URLS.FIRESTORE.API_KEY;
        return this.httpClient.post(requestURL,user);
    }

    delete(
        id:string
    ):Observable<any>{
        const requestURL = baseUrl
        +'/'+URLS.FIRESTORE.BASE_NAME
        +'/'+URLS.FIRESTORE.USERS
        +'/'+id
        +'?fields=fields,name&key='
        +URLS.FIRESTORE.API_KEY;
        return this.httpClient.delete(requestURL);
    }

    getUser(){
        return JSON.parse(localStorage.getItem('user'));
    }

}