import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { URLS } from '../app.constants';

const baseUrl = environment.rest.firestore;

@Injectable()
export class LoginProvider {
    constructor(
        private httpClient: HttpClient
    ) { };

    login(
        username: string,
        password: string
    ): Observable<any> {
        const requestURL = baseUrl
            + '/' + URLS.FIRESTORE.BASE_NAME
            + ':' + 'runQuery?fields=document&key='
            + URLS.FIRESTORE.API_KEY;
        var body = {
            structuredQuery: {
                select: {
                    fields: [
                        {
                            fieldPath: "username"
                        },
                        {
                            fieldPath: "email"
                        }
                    ]
                },
                from: [
                    {
                        collectionId: "users"
                    }
                ],
                where: {
                    compositeFilter: {
                        op: "AND",
                        filters: [
                            {
                                fieldFilter: {
                                    field: {
                                        fieldPath: "username"
                                    },
                                    op: "EQUAL",
                                    value: {
                                        stringValue: username
                                    }
                                }
                            },
                            {
                                fieldFilter: {
                                    field: {
                                        fieldPath: "password"
                                    },
                                    op: "EQUAL",
                                    value: {
                                        stringValue: password
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        };
        //{{urlbase}}/projects/mal-client-demo/databases/(default)/documents:runQuery?fields=document&key={{api_key}}
        return this.httpClient.post(requestURL, JSON.stringify(body));
    }
}