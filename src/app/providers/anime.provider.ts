import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { URLS } from '../app.constants';
import { Season } from '../models/Season.model';
import { AnimeCategory } from '../models/AnimeCategory.model';

const baseUrl = environment.rest.jikan;

@Injectable()
export class AnimeProvider {
    constructor(
        private httpClient: HttpClient
    ) { };

    getByID(
        id: number
    ): Observable<any> {
        const requestURL = baseUrl
            + '/' + URLS.JIKAN.ANIME
            + '/' + id;
        return this.httpClient.get(requestURL);
    }

    getBySeason(
        season: Season,
        year: number
    ): Observable<any> {
        const requestURL = baseUrl
            + '/' + URLS.JIKAN.SEASON
            + '/' + year
            + '/' + season;
        return this.httpClient.get(requestURL);
    }

    getTopFromCategory(
        category: AnimeCategory,
        page?: number
    ): Observable<any> {
        var requestURL = baseUrl
            + '/' + URLS.JIKAN.TOP_ANIME;
        //Request given page or default first page
        page ? requestURL += '/' + page : requestURL += '/1';
        requestURL+='/'+category;
        return this.httpClient.get(requestURL);
    }

}