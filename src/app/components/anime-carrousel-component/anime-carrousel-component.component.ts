import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Anime } from 'src/app/models/Anime.model';

@Component({
  selector: 'anime-carrousel-component',
  templateUrl: './anime-carrousel-component.component.html',
  styleUrls: ['./anime-carrousel-component.component.scss']
})
export class AnimeCarrouselComponent implements OnInit {
  @Input() animes: Anime[];
  @Output() animeSelected = new EventEmitter();
  displayedAnimes;
  //Auxiliary var for displayed elements
  display;

  constructor() { }

  ngOnInit() {
    //Var initialization
    this.displayedAnimes = [];
    this.display = {
      displayedElements: 0,
      completeDisplayedElements: 0,
      offset: 0
    };
    this.calculateElementsToDisplay();
    this.loadCarrousel(this.display.displayedElements);
  }

  onResize() {
    this.calculateElementsToDisplay();
    this.loadCarrousel(this.display.displayedElements, this.display.offset);
  }

  animeCardClicked(mal_id) {
    this.animeSelected.emit(JSON.stringify(mal_id));
  }

  loadNext(){
    this.display.offset+=this.display.completeDisplayedElements;
    this.loadCarrousel(this.display.displayedElements, this.display.offset)
  }

  loadPrevious(){
    this.display.offset-=this.display.completeDisplayedElements;
    this.loadCarrousel(this.display.displayedElements, this.display.offset)
  }

  //private functions
  private loadCarrousel(elementsNumber: number, _offset?: number) {
    var offset;
    //Declaring the offset to 0 if not given
    _offset ? offset = _offset : offset = 0;
    this.displayedAnimes = this.animes.slice(offset, elementsNumber + offset);
  };

  private calculateElementsToDisplay() {
    let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    //Calculating elements to display given the width
    //200 is the max width of the carrousel card
    this.display.displayedElements = Math.ceil(width / 200);
    this.display.completeDisplayedElements = Math.trunc(width / 200);
  }
}
