export class Anime {
    mal_id: number;
    title: string;
    image_url: string;
    episodes: number;
    type: string;
    score: number;
    status: string;

    constructor(
        mal_id?: number,
        title?: string,
        image_url?: string,
        episodes?: number,
        type?: string,
        score?: number);

    constructor(anime: any);

    constructor(
        mal_id?: number,
        title?: string,
        image_url?: string,
        episodes?: number,
        type?: string,
        score?: number,
        anime?: any
    ) {
        if (!anime) {
            mal_id ? this.mal_id = mal_id : this.mal_id = null;
            title ? this.title = title : this.title = '';
            image_url ? this.image_url = image_url : this.image_url = '';
            episodes ? this.episodes = episodes : this.episodes = 0;
            type ? this.type = type : this.type = '';
            score ? this.score = score : this.score = null;
        }
        else {
            anime.mal_id ? this.mal_id = anime.mal_id : this.mal_id = null;
            anime.title ? this.title = anime.title : this.title = '';
            anime.image_url ? this.image_url = anime.image_url : this.image_url = '';
            anime.episodes ? this.episodes = anime.episodes : this.episodes = 0;
            anime.type ? this.type = anime.type : this.type = '';
            anime.score ? this.score = anime.score : this.score = null;
        }
    }
}