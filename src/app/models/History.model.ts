type AnimeStatus =
    | 'PENDING'
    | 'WATCHING'
    | 'FINISHED';

type Scores = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

type HistoryFields = {
    mal_id: {
        integerValue: number
    },
    title: {
        stringValue: string
    },
    image_url: {
        stringValue: string
    },
    score: {
        integerValue: Scores
    },
    status: {
        stringValue: AnimeStatus
    },
    watched_episodes: {
        integerValue: number
    }
};

export class History {
    //Backend object
    fields: HistoryFields;
    //Frontend object
    mal_id: number;
    title: string;
    image_url: string;
    score: Scores;
    status: AnimeStatus;
    watched_episodes: number;
    name:string;

    // constructor(fields:any);

    // constructor(
    //     mal_id: number,
    //     title: string,
    //     image_url: string,
    //     status: AnimeStatus,
    //     watched_episodes: Scores
    // );

    constructor(
        mal_id?: number,
        title?: string,
        image_url?: string,
        score?: Scores,
        status?: AnimeStatus,
        watched_episodes?: number,
        fields?: HistoryFields,
        name?:string
    ) {
        this.name=name;
        this.fields={
            mal_id: {
                integerValue: 0
            },
            title: {
                stringValue: ''
            },
            image_url: {
                stringValue: ''
            },
            score: {
                integerValue: 0
            },
            status: {
                stringValue: 'PENDING'
            },
            watched_episodes: {
                integerValue: 0
            }
        };
        if (!fields) {
            //Backend fields
            mal_id ? this.fields.mal_id.integerValue = mal_id : null;
            title ? this.fields.title.stringValue = title : null;
            image_url ? this.fields.image_url.stringValue = image_url : this.fields.image_url.stringValue = '';
            score ? this.fields.score.integerValue = score : this.fields.score.integerValue = 0;
            status ? this.fields.status.stringValue = status : this.fields.status.stringValue = 'PENDING';
            watched_episodes ? this.fields.watched_episodes.integerValue = watched_episodes : this.fields.watched_episodes.integerValue = 0;
            //Frontend fields
            mal_id ? this.mal_id = mal_id : null;
            title ? this.title = title : null;
            image_url ? this.image_url = image_url : this.image_url = '';
            status ? this.status = status : this.status = 'PENDING';
            watched_episodes ? this.watched_episodes = watched_episodes : this.watched_episodes = 0;
        }
        else {
            //Backend fields
            this.fields = fields;
            //Frontend fields
            fields.mal_id.integerValue ? this.mal_id = parseInt(fields.mal_id.integerValue.toString()) : null;
            fields.title.stringValue ? this.title = fields.title.stringValue : null;
            fields.image_url.stringValue ? this.image_url = fields.image_url.stringValue : this.image_url = '';
            fields.score.integerValue ? this.score = fields.score.integerValue : null;
            fields.status.stringValue ? this.status = fields.status.stringValue : this.status = 'PENDING';
            fields.watched_episodes.integerValue ? this.watched_episodes = fields.watched_episodes.integerValue : this.watched_episodes = 0;
        }
    }

}