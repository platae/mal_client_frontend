export class NewUser {
	fields: {
		email: {
			stringValue: string
		},
		username: {
			stringValue: string
		},
		password: {
			stringValue: string
		}
	}
	constructor(email: string, username: string, password: string) {
		this.fields.email.stringValue = email;
		this.fields.username.stringValue = username;
		this.fields.password.stringValue = password;
	}
}