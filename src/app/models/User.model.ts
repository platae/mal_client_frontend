export class User {
	fields: {
		email: {
			stringValue: string
		},
		username: {
			stringValue: string
		}
	};
	name: string;
	constructor(email: string, username: string, name: string) {
		this.fields= {
			email: {
				stringValue: ''
			},
			username: {
				stringValue: ''
			}
		};
		this.fields.email.stringValue = email;
		this.fields.username.stringValue = username;
		this.name = name;
	}
}