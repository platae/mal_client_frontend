export const environment = {
  production: true,
  rest:{
    firestore:'https://firestore.googleapis.com/v1',
    jikan:'https://api.jikan.moe/v3'
  }
};
